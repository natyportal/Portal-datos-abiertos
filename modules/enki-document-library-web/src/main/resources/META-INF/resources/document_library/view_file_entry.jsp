<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/document_library/init.jsp" %>

<liferay-util:dynamic-include key="com.liferay.document.library.web#/document_library/view_file_entry.jsp#pre"/>

<%
    String tabs2 = ParamUtil.getString(request, "tabs2", "version-history");

    String redirect = ParamUtil.getString(request, "redirect");

    String uploadProgressId = "dlFileEntryUploadProgress";

    FileEntry fileEntry = (FileEntry) request.getAttribute(WebKeys.DOCUMENT_LIBRARY_FILE_ENTRY);

    long fileEntryId = fileEntry.getFileEntryId();

    long folderId = fileEntry.getFolderId();

    if (Validator.isNull(redirect)) {
        PortletURL portletURL = renderResponse.createRenderURL();

        portletURL.setParameter("mvcRenderCommandName", (folderId == DLFolderConstants.DEFAULT_PARENT_FOLDER_ID) ? "/document_library/view" : "/document_library/view_folder");
        portletURL.setParameter("folderId", String.valueOf(folderId));

        redirect = portletURL.toString();
    }

    Folder folder = fileEntry.getFolder();
    FileVersion fileVersion = (FileVersion) request.getAttribute(WebKeys.DOCUMENT_LIBRARY_FILE_VERSION);

    boolean versionSpecific = false;

    if (fileVersion != null) {
        versionSpecific = true;
    } else if ((user.getUserId() == fileEntry.getUserId()) || permissionChecker.isContentReviewer(user.getCompanyId(), scopeGroupId) || DLFileEntryPermission.contains(permissionChecker, fileEntry, ActionKeys.UPDATE)) {
        fileVersion = fileEntry.getLatestFileVersion();
    } else {
        fileVersion = fileEntry.getFileVersion();
    }

    long fileVersionId = fileVersion.getFileVersionId();

    com.liferay.portal.kernel.lock.Lock lock = fileEntry.getLock();

    String[] conversions = new String[0];

    if (PropsValues.DL_FILE_ENTRY_CONVERSIONS_ENABLED && PrefsPropsUtil.getBoolean(PropsKeys.OPENOFFICE_SERVER_ENABLED, PropsValues.OPENOFFICE_SERVER_ENABLED)) {
        conversions = (String[]) DocumentConversionUtil.getConversions(fileVersion.getExtension());
    }

    long assetClassPK = 0;

    if (!fileVersion.isApproved() && !fileVersion.getVersion().equals(DLFileEntryConstants.VERSION_DEFAULT) && !fileEntry.isInTrash()) {
        assetClassPK = fileVersion.getFileVersionId();
    } else {
        assetClassPK = fileEntry.getFileEntryId();
    }

    String webDavURL = StringPool.BLANK;

    if (portletDisplay.isWebDAVEnabled()) {
        webDavURL = DLUtil.getWebDavURL(themeDisplay, folder, fileEntry);
    }

    AssetEntry layoutAssetEntry = AssetEntryLocalServiceUtil.fetchEntry(DLFileEntryConstants.getClassName(), assetClassPK);

    request.setAttribute(WebKeys.LAYOUT_ASSET_ENTRY, layoutAssetEntry);

    DLPortletInstanceSettingsHelper dlPortletInstanceSettingsHelper = new DLPortletInstanceSettingsHelper(dlRequestHelper);
    final DLViewFileVersionDisplayContext dlViewFileVersionDisplayContext = dlDisplayContextProvider.getDLViewFileVersionDisplayContext(request, response, fileVersion);

    boolean portletTitleBasedNavigation = GetterUtil.getBoolean(portletConfig.getInitParameter("portlet-title-based-navigation"));

    if (portletTitleBasedNavigation) {
        portletDisplay.setShowBackIcon(true);
        portletDisplay.setURLBack(redirect);

        renderResponse.setTitle(fileVersion.getTitle());
    }
%>

<div class="closed container-fluid-1280 sidenav-container sidenav-right ficha" id="<portlet:namespace />infoPanelId">
    <portlet:actionURL name="/document_library/edit_file_entry" var="editFileEntry"/>

    <aui:form action="<%= editFileEntry %>" method="post" name="fm">
        <aui:input name="<%= Constants.CMD %>" type="hidden"/>
        <aui:input name="redirect" type="hidden" value="<%= currentURL %>"/>
        <aui:input name="fileEntryId" type="hidden" value="<%= String.valueOf(fileEntry.getFileEntryId()) %>"/>
    </aui:form>

    <c:if test="<%= !portletTitleBasedNavigation && showHeader && (folder != null) %>">
        <liferay-ui:header
                backURL="<%= redirect %>"
                localizeTitle="<%= false %>"
                title="<%= fileVersion.getTitle() %>"
        />
    </c:if>

    <%
        request.removeAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    %>


    <div class="sidenav-content">
        <div class="alert alert-danger hide" id="<portlet:namespace />openMSOfficeError"></div>

        <c:if test="<%= (fileEntry.getLock() != null) && DLFileEntryPermission.contains(permissionChecker, fileEntry, ActionKeys.UPDATE) %>">
            <c:choose>
                <c:when test="<%= fileEntry.hasLock() %>">
                    <div class="alert alert-success">
                        <c:choose>
                            <c:when test="<%= lock.isNeverExpires() %>">
                                <liferay-ui:message key="you-now-have-an-indefinite-lock-on-this-document"/>
                            </c:when>
                            <c:otherwise>

                                <%
                                    String lockExpirationTime = StringUtil.toLowerCase(LanguageUtil.getTimeDescription(request, DLFileEntryConstants.LOCK_EXPIRATION_TIME));
                                %>

                                <liferay-ui:message arguments="<%= lockExpirationTime %>"
                                                    key="you-now-have-a-lock-on-this-document"
                                                    translateArguments="<%= false %>"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="alert alert-danger">
                        <liferay-ui:message
                                arguments="<%= new Object[] {HtmlUtil.escape(PortalUtil.getUserName(lock.getUserId(), String.valueOf(lock.getUserId()))), dateFormatDateTime.format(lock.getCreateDate())} %>"
                                key="you-cannot-modify-this-document-because-it-was-locked-by-x-on-x"
                                translateArguments="<%= false %>"/>
                    </div>
                </c:otherwise>
            </c:choose>
        </c:if>

        <liferay-util:buffer var="documentTitle">
            <%= fileVersion.getTitle() %>

            <c:if test="<%= versionSpecific %>">
                (<liferay-ui:message key="version"/> <%= fileVersion.getVersion() %>)
            </c:if>
        </liferay-util:buffer>

        <div class="f-header">
            <h2>Ficha</h2>
            <h3 title="<%= HtmlUtil.escapeAttribute(documentTitle) %>">
                <%= HtmlUtil.escape(documentTitle) %>
            </h3>
            <span>Calificaci&oacute;n</span>
            <c:if test="<%= dlPortletInstanceSettings.isEnableRatings() && fileEntry.isSupportsSocial() %>">
						<span class="f-ratings">
							<liferay-ui:ratings
                                    className="<%= DLFileEntryConstants.getClassName() %>"
                                    classPK="<%= fileEntryId %>"
                            />
						</span>
            </c:if>
        </div>
        <div class="f-detail row">
            <div class="col-sm-12 col-md-3 col-lg-4 f-meta">
                <div>
                    <span>Etiquetas</span>
                    <liferay-ui:asset-tags-summary
                            className="<%= DLFileEntryConstants.getClassName() %>"
                            classPK="<%= assetClassPK %>"
                            message=""
                    />
                </div>

                <br/>
                <div>
                    <span>Fecha Publicaci&oacute;n</span>
                    <span><%=dateFormatDateTime.format(fileEntry.getCreateDate())%></span>
                </div>

                <br/>
                <div>
                    <span>Fecha Actualizaci&oacute;n</span>
                    <span><%=dateFormatDateTime.format(fileEntry.getModifiedDate())%></span>
                </div>

                <br/>
                <div class="f-meta-2">
                    <%
                        DDMFormValues ddmFormValuesActualizacion = null;
                        DDMFormValues ddmFormValuesContenido = null;
                        DDMStructure ddmStructure = null;
                    %>
                    <c:if test="<%= dlViewFileVersionDisplayContext.getDDMStructuresCount() > 0 %>">

                        <%

                            //referencia https://github.com/liferay/liferay-portal/blob/master/modules/apps/forms-and-workflow/dynamic-data-mapping/dynamic-data-mapping-api/src/main/java/com/liferay/dynamic/data/mapping/storage/DDMFormValues.java
                            //https://docs.liferay.com/portal/7.0/javadocs/portal-kernel/com/liferay/dynamic/data/mapping/kernel/DDMFormValues.html#getDDMFormFieldValuesMap--
                            //https://docs.liferay.com/portal/7.0/javadocs/portal-kernel/com/liferay/dynamic/data/mapping/kernel/DDMFormFieldValue.html
                            //para metadata izquierda
                            try {
                                List<DDMStructure> ddmStructures = dlViewFileVersionDisplayContext.getDDMStructures();

                                for (DDMStructure _ddmStructure : ddmStructures) {
                                    ddmStructure = _ddmStructure;
                                    DDMFormValues ddmFormValues = null;


                                    //List<DDMFormFieldValue> ddmFormFieldValues1 = new ArrayList<DDMFormFieldValue>();
                                    //List<DDMFormFieldValue> ddmFormFieldValues2 = new ArrayList<DDMFormFieldValue>();

                                    try {
                                        ddmFormValues = dlViewFileVersionDisplayContext.getDDMFormValues(ddmStructure);

                                        //obtengo el map de clave valor de la estructura document type (actualizacion, contenido)
                                        Map<String, List<DDMFormFieldValue>> map1 = ddmFormValues.getDDMFormFieldValuesMap();


                                        //clono para cada elemento

                                        // pongo el nuevo mapa en la clase original si existe
                                        if (map1.containsKey("Actualizacion")) {
                                            ddmFormValuesActualizacion = new DDMFormValues(ddmFormValues.getDDMForm());
                                            ddmFormValuesActualizacion.setDefaultLocale(ddmFormValues.getDefaultLocale());
                                            ddmFormValuesActualizacion.setDDMFormFieldValues(map1.get("Actualizacion"));
                                        }

                                        if (map1.containsKey("Contenido")) {
                                            ddmFormValuesContenido = new DDMFormValues(ddmFormValues.getDDMForm());
                                            ddmFormValuesContenido.setDefaultLocale(ddmFormValues.getDefaultLocale());
                                            ddmFormValuesContenido.setDDMFormFieldValues(map1.get("Contenido"));
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace(response.getWriter());
                                        //response.getWriter().print
                                    }
                        %>


                        <c:if test="<%= ddmFormValuesActualizacion !=null && !ddmFormValuesActualizacion.getDDMFormFieldValues().isEmpty() %>">

                            <liferay-ddm:html
                                    classNameId="<%= PortalUtil.getClassNameId(com.liferay.dynamic.data.mapping.model.DDMStructure.class) %>"
                                    classPK="<%= ddmStructure.getPrimaryKey() %>"
                                    ddmFormValues="<%= ddmFormValuesActualizacion %>"
                                    fieldsNamespace="<%= String.valueOf(ddmStructure.getPrimaryKey()) %>"
                                    readOnly="<%= true %>"
                                    requestedLocale="<%= (ddmFormValuesActualizacion != null) ? ddmFormValuesActualizacion.getDefaultLocale() : locale %>"
                                    showEmptyFieldLabel="<%= false %>"
                            />
                        </c:if>


                        <%
                                }
                            } catch (Exception e) {
                                e.printStackTrace(response.getWriter());
                            }
                        %>

                    </c:if>
                </div>

            </div>
            <div class="col-sm-12 col-md-9 col-lg-8 f-content">
                <c:if test="<%= ddmFormValuesContenido !=null && !ddmFormValuesContenido.getDDMFormFieldValues().isEmpty() %>">

                    <liferay-ddm:html
                            classNameId="<%= PortalUtil.getClassNameId(com.liferay.dynamic.data.mapping.model.DDMStructure.class) %>"
                            classPK="<%= ddmStructure.getPrimaryKey() %>"
                            ddmFormValues="<%= ddmFormValuesContenido %>"
                            fieldsNamespace="<%= String.valueOf(ddmStructure.getPrimaryKey()) %>"
                            readOnly="<%= true %>"
                            requestedLocale="<%= (ddmFormValuesContenido != null) ? ddmFormValuesActualizacion.getDefaultLocale() : locale %>"
                            showEmptyFieldLabel="<%= false %>"
                    />
                </c:if>
                <c:if test="<%= ddmFormValuesContenido ==null || ddmFormValuesContenido.getDDMFormFieldValues().isEmpty() %>">
                    <liferay-ui:empty-result-message
                            message="ning&uacute;n contenido web"
                    />
                </c:if>
                <br/>
                <div class="f-download">
                    <div class="f-d-desc">
                        Descargar
                    </div>

                    <a href="<%= DLUtil.getDownloadURL(fileEntry, fileVersion, themeDisplay, StringPool.BLANK) %>"
                       target="_blank" class="hidden">
                        <%= LanguageUtil.get(resourceBundle, "download") + " (" + TextFormatter.formatStorageSize(fileVersion.getSize(), locale) + ")" %>
                    </a>

                    <a href="<%= DLUtil.getDownloadURL(fileEntry, fileVersion, themeDisplay, StringPool.BLANK) %>"
                       target="_blank">
                        <%= fileVersion.getExtension() + " (" + TextFormatter.formatStorageSize(fileVersion.getSize(), locale) + ")" %>
                    </a>


                </div>
                <div class="f-related">
                    <%
                        if (layoutAssetEntry != null) {
                            AssetEntry incrementAssetEntry = AssetEntryServiceUtil.incrementViewCounter(layoutAssetEntry.getClassName(), fileEntry.getFileEntryId());

                            if (incrementAssetEntry != null) {
                                layoutAssetEntry = incrementAssetEntry;
                            }
                        }
                    %>

                    <c:if test="<%= (layoutAssetEntry != null) && dlPortletInstanceSettings.isEnableRelatedAssets() && fileEntry.isSupportsSocial() %>">
                        <div class="entry-links">
                            <liferay-ui:asset-links
                                    assetEntryId="<%= layoutAssetEntry.getEntryId() %>"
                            />
                        </div>
                    </c:if>
                </div>
            </div>
        </div>

        <div class="row od-comments">
            <div class="col-md-12">
                <c:if test="<%= showComments && fileEntry.isRepositoryCapabilityProvided(CommentCapability.class) %>">
                    <liferay-ui:panel collapsible="<%= true %>" cssClass="lfr-document-library-comments"
                                      extended="<%= true %>" markupView="lexicon" persistState="<%= true %>"
                                      title="<%= dlViewFileVersionDisplayContext.getDiscussionLabel(locale) %>">
                        <liferay-ui:discussion
                                className="<%= dlViewFileVersionDisplayContext.getDiscussionClassName() %>"
                                classPK="<%= dlViewFileVersionDisplayContext.getDiscussionClassPK() %>"
                                formName="fm2"
                                ratingsEnabled="<%= dlPortletInstanceSettings.isEnableCommentRatings() %>"
                                redirect="<%= currentURL %>"
                                userId="<%= fileEntry.getUserId() %>"
                        />
                    </liferay-ui:panel>
                </c:if>
            </div>
        </div>

    </div>
</div>

<c:if test="<%= dlPortletInstanceSettingsHelper.isShowActions() %>">

    <%
        request.setAttribute("edit_file_entry.jsp-checkedOut", fileEntry.isCheckedOut());
    %>

    <liferay-util:include page="/document_library/version_details.jsp" servletContext="<%= application %>"/>
</c:if>

<aui:script>
    function <portlet:namespace/>compare() {
    var rowIds = AUI.$('input[name=<portlet:namespace/>rowIds]:checked');
    var sourceFileVersionId = AUI.$('input[name="<portlet:namespace/>sourceFileVersionId"]');
    var targetFileVersionId = AUI.$('input[name="<portlet:namespace/>targetFileVersionId"]');

    var rowIdsSize = rowIds.length;

    if (rowIdsSize == 1) {
    sourceFileVersionId.val(rowIds.eq(0).val());
    }
    else if (rowIdsSize == 2) {
    sourceFileVersionId.val(rowIds.eq(1).val());

    targetFileVersionId.val(rowIds.eq(0).val());
    }

    submitForm(document.<portlet:namespace/>fm1);
    }

    function <portlet:namespace/>initRowsChecked() {
    AUI.$('input[name=<portlet:namespace/>rowIds]').each(
    function(index, item) {
    if (index >= 2) {
    item = AUI.$(item);

    item.prop('checked', false);
    }
    }
    );
    }

    function <portlet:namespace/>updateRowsChecked(element) {
    var rowsChecked = AUI.$('input[name=<portlet:namespace/>rowIds]:checked');

    if (rowsChecked.length > 2) {
    var index = 2;

    if (rowsChecked.eq(2).is(element)) {
    index = 1;
    }

    rowsChecked.eq(index).prop('checked', false);
    }
    }
</aui:script>

<c:if test="<%= DLFileEntryPermission.contains(permissionChecker, fileEntry, ActionKeys.VIEW) && DLUtil.isOfficeExtension(fileVersion.getExtension()) && portletDisplay.isWebDAVEnabled() && BrowserSnifferUtil.isIeOnWin32(request) %>">
    <%@ include file="/document_library/action/open_document_js.jspf" %>
</c:if>

<aui:script sandbox="<%= true %>">
    $('.show-url-file').on(
    'click',
    function(event) {
    $('.url-file-container').toggleClass('hide');
    }
    );

    $('.show-webdav-url-file').on(
    'click',
    function(event) {
    $('.webdav-url-file-container').toggleClass('hide');
    }
    );

    <portlet:namespace/>initRowsChecked();

    $('input[name=<portlet:namespace/>rowIds]').on(
    'click',
    function(event) {
    <portlet:namespace/>updateRowsChecked($(event.currentTarget));
    }
    );
</aui:script>

<%
    boolean addPortletBreadcrumbEntries = ParamUtil.getBoolean(request, "addPortletBreadcrumbEntries", true);

    if (addPortletBreadcrumbEntries) {
        DLBreadcrumbUtil.addPortletBreadcrumbEntries(fileEntry, request, renderResponse);
    }
%>

<liferay-util:dynamic-include key="com.liferay.document.library.web#/document_library/view_file_entry.jsp#post"/>
